#include <iostream>
#include <vector>

using namespace std;
int n;

void MergeSortImpl(vector<int> &mas, vector<int> &buffer, int l, int r) {
    if (l < r) {
        int m;
        m = (l + r) / 2;
        MergeSortImpl(mas, buffer, l, m);
        MergeSortImpl(mas, buffer, m + 1, r);
        int k = l;
        for (int i = l, j = m + 1; i <= m || j <= r;) {
            if (j > r || (i <= m && mas[i] < mas[j])) {
                buffer[k] = mas[i];
                i++;
            } else {
                buffer[k] = mas[j];
                j++;
            }
            k++;
        }
        for (int i = l; i <= r; ++i) {
            mas[i] = buffer[i];
        }
    }
}


void PrintArray(vector<int> &mas, int n){
    for (int i=0; i<n; ++i)
        cout << mas[i] << " ";
    cout << endl;
}


void MergeSort(vector<int> &mas){
    if(!mas.empty()){
        vector<int> buffer(mas.size());
        MergeSortImpl(mas, buffer, 0, mas.size() - 1);
    }
}



int main()
{
    std::cout<<"Количество элементов в последовательности: ";
    std::cin>> n;
    vector<int> mas = {};

    for(int i = 0; i < n; i ++){
        mas.push_back(rand()%30);
    }

    MergeSort(mas);

    PrintArray(mas,mas.size());

    return 0;
}