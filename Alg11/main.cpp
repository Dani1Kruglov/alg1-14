#include <iostream>

int n;

void QUICKSORT(int mas[n], int a, int b)
{
    if(a >=b)
        return;
    int m, k, l, r;
    m =  rand() % (b - a + 1) + a;
    k = mas[m];
    l = a - 1;
    r = b + 1;
    while(1)
    {
        do l = l + 1; while (mas[l] < k);
        do r = r - 1; while (mas[r] > k);
        if( l >= r)
            break;
        std::swap(mas[l],mas[r]);
    }
    r = l;
    l = l - 1;
    QUICKSORT( mas, a, l);
    QUICKSORT( mas, r, b);

}


int main()
{
    std::cout<<"Количество элементов в последовательности: ";
    std::cin>> n;
    int mas[n];
    for(int i = 0; i < n; i ++)
        mas[i] = rand()%50;
    int a = 0;
    int b = n - 1;
    QUICKSORT(mas, a, b);
    for(int i = 0; i < n; i ++)
        std::cout<<mas[i]<<" ";
    return 0;
}


