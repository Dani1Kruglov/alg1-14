#include "iostream"

using namespace std;
const int N=10;

int SizeRazr(int value,int razr)
{
    while(razr>1)
    {
        value = value /10;
        razr--;
    }
    value = value % 10;
    return value;
}

void SortRazr(int addmas[N][N], int mas[N], int razr)
{
    int mas_col[N] = {0,0,0,0,0,0};
    int temp = 0;

    for(int i=0; i<N; i++){
        int a = SizeRazr(mas[i], razr);
        addmas[mas_col[a]][a]=mas[i];
        mas_col[a]++;
    }
    for(int i=0; i<N; i++){
        for(int j=0; j<mas_col[i]; j++){
            mas[temp]=addmas[j][i];
            temp++;
        }
    }
}

int main()
{

    int razr, i;
    int mas[N]={576,385,304,904,376,584,100,675,284,493};

    int addmas[N][N];

    for(razr=1; razr<4; razr++)
        SortRazr(addmas, mas, razr);

    for(i=0; i<N; i++)
        cout<<mas[i]<<" ";
    cout<<endl;

    return 0;
}