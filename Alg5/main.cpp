#include <iostream>
#include <vector>

using namespace std;
int n;


void InsertionSort(vector<int> &mas){
    for(int i = 1 ; i < mas.size(); i++){
        int x = mas[i];
        int j = i;
        while(j > 0 && mas[j-1] > x){
            mas[j] = mas[j - 1];
            j--;
        }
        mas[j] = x;
    }
}

int main()
{
    std::cout<<"Количество элементов в последовательности: ";
    std::cin>> n;
    vector<int> mas = {};
    for(int i = 0; i < n; i ++){
        mas.push_back(rand()%30);
    }

    InsertionSort(mas);

    for(int i =0; i < n; i++)
        cout<<mas[i]<<" ";
    cout<<endl;

    return 0;
}
