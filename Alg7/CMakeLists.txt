cmake_minimum_required(VERSION 3.21)
project(Alg7)

set(CMAKE_CXX_STANDARD 20)

add_executable(Alg7 main.cpp)
