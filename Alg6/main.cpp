#include <iostream>
#include <vector>


using namespace std;

int SelectionSort(vector <int> &mas){
    for(int i = 0; i < mas.size() - 1; i++) {
        int min_index = i;
        for (int j = i + 1; j < mas.size(); j++)
            if (mas[j] < mas[min_index])
                min_index = j;
        if (i != min_index)
            swap(mas[i], mas[min_index]);
    }
}

void PrintArray(vector<int> &mas, int n){
    for (int i=0; i<n; ++i)
        cout << mas[i] << " ";
    cout << endl;
}


int main()
{
    int n;
    std::cout<<"Количество элементов в последовательности: ";
    std::cin>> n;
    vector<int> mas = {};

    for(int i = 0; i < n; i ++){
        mas.push_back(rand()%30);
    }

    SelectionSort(mas);

    PrintArray(mas,n);

    cout<<endl;
    return 0;
}

