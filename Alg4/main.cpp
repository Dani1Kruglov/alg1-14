#include <iostream>
#include <vector>

using namespace std;
int n;

void TdCombSort(vector<int> &mas)
{
    float factor = 1.3;
    int step = mas.size() - 1;
    while(step >= 1){

        if((step == 10) || (step == 9))
            step = 11;

        for(int i =0; i + step < mas.size(); i++){
            if(mas[i] > mas[i+step])
                swap(mas[i], mas[ i + step]);
        }
        step = step/factor;
    }
}


int main()
{
    std::cout<<"Количество элементов в последовательности: ";
    std::cin>> n;
    vector<int> mas = {};
    for(int i = 0; i < n; i ++){
        mas.push_back(rand()%30);
    }

    TdCombSort(mas);

    for(int i =0; i < n; i++)
        cout<<mas[i]<<" ";
    cout<<endl;

    return 0;
}
